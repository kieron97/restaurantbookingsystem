import java.util.Scanner;  // Import the Scanner class
public class UI {
	static Scanner Input = new Scanner(System.in);  // Create a Scanner object
	static String Name;
	static void Welcome_Message() {
		System.out.println("==================================");// gives the user feedback and this is what is seen when opening the application
		System.out.println("Hello Please enter your name");
		System.out.println("==================================");//GOAL 4 is met here
		Name = Input.nextLine();  // Read user input
		System.out.println("==================================");
		System.out.println("Welcome " + Name );// takes the users input and displays it giving the user more feedback
		System.out.println("==================================");
	}
	static void Seat_Option() {// Goal 5 is met here
		System.out.println("==================================");
		System.out.println("Would u like to pick your own seat");
		System.out.println("Or have one alocated ");
		System.out.println("1 (Pick your own) 2 (Allocated)");
		System.out.println("==================================");
	}
	static void Seat_input_Message() {
		System.out.println("=================================");// this is a message what may need to be displayed multiple times during the run of the program so whenever i need it i will call it from here
		System.out.println("Please enter the number of your");
		System.out.println("seat eg (1 , 17, 42");
		System.out.println("Seats marked with an XX have been");
		System.out.println("reserved.");
		System.out.println("=================================");
		
	}
	static void Seat_Accepted() {// this appears when the user picks a free seat
		System.out.println("=================================");
		System.out.println("Congratulations your seat is ");
		System.out.println("booked");
		System.out.println("=================================");
	}	
	static void Seat_Taken() {//this appears when the user picks a seat what is already claimed 
		System.out.println("=================================");
		System.out.println("The seat you selected has already");
		System.out.println("been reserved");
		System.out.println("=================================");
	}
	static void Error01() {// this error appears if they select an option that does not exist
		System.out.println("=================================");
		System.out.println("ERROR 01:");
		System.out.println("You have entered an option what");
		System.out.println("does not exist");
		System.out.println("please try another option");
		System.out.println("=================================");
	}
	static void Error02() {// this error appears if an item is out of stock
		System.out.println("=================================");
		System.out.println("ERROR 02:");
		System.out.println("You have entered an option what");
		System.out.println("is out of stock");
		System.out.println("please try another option");
		System.out.println("=================================");
	}
	static void Display_Basket() {// GOAL 9
		System.out.println("=================================");
		for (int i = 1 ; i< Data.Item_Counter ; i ++) {// for every item in the basket
			System.out.println(i + "   " + Basket.item[i]);//print the item in the basket
		}
		System.out.println("=================================");// this is just feedback for the user to see
		System.out.println("Select What Item to remove");
		System.out.println("Or select 0 to return to menu");
		System.out.println("=================================");
	}

	static void Main_Menu() {//GOAL 8 is met here
		System.out.println("==================================");// this is the page where most actions can be done 
		System.out.println("Main Menu");
		System.out.println("What would you like to do:");
		System.out.println("Select an item press 1");
		System.out.println("Remove an item press 2");
		System.out.println("Checkout press 3");
		System.out.println("Exit press 0");
		System.out.println("==================================");
	}
	static void Confirm_Checkout() {//made to make sure the user wants to actually checkout
		System.out.println("==================================");
		System.out.println("Are you sure you want to check out ?");
		System.out.println("1 yes 0 no");
		System.out.println("==================================");
	}
	static void Make_Payment() {// this method makes it possible for the user to pay the price for the meal
		double pay;// GOAL 10
		
		while (Data.SUB > 0) {
		System.out.println("==================================");// payment gets made
		System.out.println("You still have to pay �" + Data.SUB);
		System.out.println("Enter the amount you will pay");
		pay = Double.parseDouble(Input.nextLine());
		Data.SUB = Data.SUB -pay;
		System.out.println("==================================");
		}
		if (Data.SUB < 0) {
			System.out.println("==================================");
			System.out.println(Data.SUB + " is your change enjo your meal");// change given
			System.out.println("==================================");
		}
	}
	static void Get_Checkout(){
		
		System.out.println("==================================");
		System.out.println(Name + "    " + Data.Choice_Seat);// Goal 11
		for (int i = 1 ; i< Data.Item_Counter ; i ++) {// for every item in the basket
			System.out.println(i + "   " + Basket.item[i]);//print the item in the basket	
		}
		System.out.println("Total-----------" + Data.SUB);
		System.out.println("==================================");
	}
	static void Display_Menu() {// displays the menu where food will be bought 
		System.out.println("==================================");//GOAL 7 IS MET
		for (int i = 1; i < 10;i++) {// goes through every item in the list
			if(i == 1) {
			System.out.println(Menu.Code[i] + "  " + Menu.Food[i] + "  �" + Menu.Price[i] + "  " + Menu.Quantity[i]);// displays the items  the menu neatly
			}
			else {
			System.out.println(Menu.Code[i] + "  " + Menu.Food[i] + "  �" + Menu.Price[i] + "   " + Menu.Quantity[i]);
		}
		}
		System.out.println("0  Return to main menu");
		System.out.println("==================================");
	}
	static void Menu_Message() {// sends out a message when the item is out of stock
		System.out.println("==================================");
		System.out.println("Please enter the number beside the");
		System.out.println("item you want to select");
		System.out.println("==================================");
	}
}
