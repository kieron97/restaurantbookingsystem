import java.util.Scanner;  // Import the Scanner class
public class Seats {
	Scanner Input = new Scanner(System.in);  // Create a Scanner object
	static Scanner input = new Scanner(System.in);  // Create a Scanner object
	static String Seat[] = {"N/A","XX","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31","32","33","34","35","36","37","38","39","40","41","42"};
	// makes each seat its own part in the array
	//GOAL 2 is met here
	// the static makes the variable be called by different classes
	static void Display_Seats() {
		System.out.println("==================================");// GOAL 1 is met here
		for (int i = 1 ; i < 43 ; i++) {
			if (i == 7||i == 21|| i == 35|| i == 42) {// makes the new line after the 7th 21st  35th and 42nd seat
				System.out.println("[" + Seat[i] + "] ");// the space after the ] makes it the correct layout
			}
			else if (i == 14 || i == 28) {
				System.out.println("[" + Seat[i] + "] ");// the space after the ] makes it the correct layout
				System.out.println("");//makes the double new line needed for the correct layout
			}
			else {
				System.out.print("[" + Seat[i] + "] ");// the space after the ] makes it the correct layout
			}		
			}
		System.out.println("=================================");
		UI.Seat_input_Message();
		}
	static void Get_Seat(int x) {//this is the function where they order a seat
		boolean lock = true;// this acts like a key . the only way out is to do this correctly
		while ( lock == true) {
			if (x > 42 || x <1) {//checks if the seat is an actual seat
				UI.Error01();
				UI.Seat_input_Message();
				x = Integer.parseInt(input.nextLine());//converts the input to string 
			}
			else {
		if (Seat[x] == "XX") {//GOAL 3 is met here
			UI.Seat_Taken();//displays the seat taken message GOAL 6 is done here
			UI.Seat_input_Message();//displays the seat input message
			x = Integer.parseInt(input.nextLine());//converts the input to string 
		}
		else {
			Seat[x] = "XX";
			lock = false;	
			UI.Seat_Accepted();
		}
		}
		}
	}
}
