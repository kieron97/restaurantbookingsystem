import java.util.Scanner;  // Import the Scanner class
public class Data {
	static String replacement;
	static Scanner Input = new Scanner(System.in);  // Create a Scanner object
	static int Choice_Seat;
	static double SUB;// shows the end price
	static int Item_Counter = 1;// this is used for the basket class when listing items in their basket
	static double SUB_Item[] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	static int SUB_Counter= 0;
	public static void main(String[] args) {
	boolean running =  true;//shows if the WHOLE app is active
	boolean User = true;//shows if the user is active 
	int Choice_Main;// this will be used for the main menu navigation
	int Choice_Meal;// this will be used to select the item off the menu
	int Choice_Removal;
	int Removal = 0; // this is for removing items
		while (running == true) {
			UI.Welcome_Message();// calls the class UI to display the welcome message
			Seats.Display_Seats();// displays the seats
			UI.Seat_Option();
			Choice_Main = Integer.parseInt(Input.nextLine());
			if (Choice_Main == 1) {//if they chose to pick their own seat
			  Choice_Seat = Integer.parseInt(Input.nextLine());  // Read user input and converts it to integer for the next function
			Seats.Get_Seat(Choice_Seat);// using choice it will go through the get seat function
			User = true;
			}
			else {
				for (int i = 1 ; i < 43; i++) {
					if (Seats.Seat[i] == "XX") {// makes the seat taken
				}
					else {
						Seats.Seat[i] = "XX";
						i = 100;//breaks out the loop
						UI.Seat_Accepted();
					}
			}
			while(User == true) {
				UI.Main_Menu();//displays the main menu 
				 Choice_Main = Integer.parseInt(Input.nextLine()) ;// the users input to the main menu
				if (Choice_Main == 1) {// what happens when the select "Select item"
					UI.Display_Menu();	//displays the food menu GOAL 7 is done here
					UI.Menu_Message();//displays the menu message
					Choice_Meal =  Integer.parseInt(Input.nextLine()) ;
					if (Choice_Meal == 0 ){
			// because nothing happens in here the program will return them to the main menu			
					}
					else if (Choice_Meal > 9) {//checks if the item picked is actually on the menu
						UI.Error01();
					}
					else if (Menu.Quantity[Choice_Meal] < 0) {// checks if the item is in stock
						UI.Error02();
					}
					else {
						Basket.item[Item_Counter] = Menu.Food[Choice_Meal] +"  �" + Menu.Price[Choice_Meal];//GOAL 9
						Item_Counter++;
						Menu.Quantity[Choice_Meal]--;// lowers the quantity of the item chosen 
						SUB = SUB + Menu.Price[Choice_Meal];// ups the price of the order
						SUB_Item[SUB_Counter] = Menu.Price[Choice_Meal];
						SUB_Counter++;
					}
				}
				else if (Choice_Main == 2) {//what happens when they select "Remove Item"
					UI.Display_Basket();//GOAL 9 
					Choice_Removal =  Integer.parseInt(Input.nextLine()) ;
					if(Choice_Removal ==0) {}//moves the user back to the main menu
					else if(Choice_Removal > 0 && Choice_Removal < Item_Counter) {// if its an item
					while(Removal < Item_Counter + 1) {
						if (Removal == Choice_Removal || Removal > Choice_Removal){
							Basket.item[Removal] = Basket.item[Removal + 1];// replaces the selected item with the item infront 
							SUB = SUB - SUB_Item[Removal];
							
						}
						Removal++;
						}// The process is to remove the item
					Item_Counter--;
					}
					else {// if they select an item what isnt listed
						UI.Error01();
					}
				}
				else if (Choice_Main == 3) {//what happens when they select "Checkout"
					UI.Confirm_Checkout();// makes sure the user wants to actually checkout
					Choice_Main = Integer.parseInt(Input.nextLine()) ;// the users input to the main menu
					if (Choice_Main == 1) {
					UI.Get_Checkout();//does the check out method
					for (int i = 0; i < Item_Counter;i++) {
						Basket.item[i] ="";
					}
					UI.Make_Payment();
					Item_Counter = 1;
					User = false;
					}
				}
				else if (Choice_Main == 0) {//what happens when they select "Exit"
					if (Choice_Seat < 10) {
						replacement =  "0" + Choice_Seat;
						}
					else {
						replacement = Integer.toString(Choice_Seat);
						}
					User = false;
					Seats.Seat[Choice_Seat] = replacement;// makes the seat back to its original state
					}
				
				}
			}
		}
	}
}
